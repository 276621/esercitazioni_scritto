# Prova scritta di programmazione 2 - 17/02/2022

## Domanda 1 - 2 punti
- [x] a
- [ ] b
- [x] c
- [ ] d

## Domanda 2 - 2 punti
- [x] a
- [x] b
- [x] c
- [ ] d

## Domanda 3 - 2 punti
- [ ] a
- [x] b
- [ ] c
- [x] d

## Domanda 4 - 3 punti
```
main:  
    f(5):
        return f(4):
            return f(3):
                "3 "
                f(f3(3)):
                    f(
                        return f3(2):
                            "2 "
                            return 1                    
                    )
                    f(1):
                        return f2(0):
                            "0 "
                            return -1                    
```

``
Risultato: 3 2 0 -1
``

## Domanda 5 - 3 punti
```
    int a = [0,1,1,0]
    f(f1,f2,a,4):
    f(*fun1, *fun2, *v, n):
        for i=0 to 3:
            i=0:
                v[0] = f1(v[0]+1) = 1 //a -> [1,1,1,0]
                    f1(v[0]+1):
                        return 1*1 = 1
            i=1:
                v[1] = f1(v[1]+1) = 4 //a -> [1,4,1,0]
                    f1(v[1]+1):
                        return 2*2 = 4
            i=2:
                v[2] = f1(v[2]+1) = 4 //a -> [1,4,4,0]
                    f1(v[2]+1):
                        return 2*2 = 4
            i=3:
                v[3] = f2(v[3]+v[3-2]) = 8 //a -> [1,4,1,8]
                    f2(4):
                        return 4*2 = 8       
```

``
Risultato: "1 4 1 8"
``

## Domanda 6 - 3 punti

```
main:
    int a = [1,4,2,3,9]
    for i = 0 to 3:
        i=0:
            a[0] = f2(a[0])
                f2(1):
                    return 1-1 = 0

            a[0] = a[0] | a[0+1] = 00000000 | 00000100
        i=1:
            a[1] = f1(1):
                ...

```

``
XXX: ???
``

``
YYY: return n-n
``

## Domanda 7 - 5 punti

```cpp
lista somma_elemento(lista l, int p)
{
    lista att = l;
    while(att!=NULL && att->inf!=p)
    {
        att = att->pun;
    }
    int offset = att->inf;

    while(l!=NULL)
    {
        l->inf += offset;
        l = l->pun;
    }

    return l;
}
```

## Domanda 8 - 6 punti

```cpp
bool palindroma(lista l)
{
    lista p = l
    int n_elem = 0;

    if(l==NULL)
        return false;

    while(p->pun!=NULL)
    {
        p = p->pun;
        n_elem++;
    }
    lista r = p;
    p = l;

    while((p!=r->prev && r!=p->pun) || p!=r)
    {
        if(p->inf != r->inf)
            return false;
    }

    return true;
}
```

## Domanda 9 - 6 punti

```cpp
//
```