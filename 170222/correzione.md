# Prova 17/02/22
## _Punteggio finale_

| Esercizio | Punteggio |
|:---------:|:---------:|
|     1     |     2     |
|     2     |     2     |
|     3     |     2     |
|     4     |     0     |
|     5     |     0     |
|     6     |     0     |
|     7     |     5     |
|     8     |     6     |
|     9     |     0     |
|   Totale  |     **17**    |