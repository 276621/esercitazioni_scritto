# Prova 17/02/22
## _Punteggio finale_

| Esercizio | Punteggio |
|:---------:|:---------:|
|     1     |     1,3     |
|     2     |     1     |
|     3     |     1     |
|     4     |     3     |
|     5     |     3     |
|     6     |     3     |
|     7     |     6     |
|     8     |     4     |
|     9     |     4     |
|   Totale  |     **26**    |