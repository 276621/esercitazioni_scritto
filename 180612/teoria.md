# Prova scritta di programmazione 2 - 17/02/2022

## Domanda 1 - 2 punti
- [ ] a
- [x] b
- [x] c
- [ ] d

## Domanda 2 - 2 punti
- [ ] a
- [x] b
- [x] c
- [ ] d

## Domanda 3 - 2 punti
- [ ] a
- [x] b
- [x] c
- [ ] d

## Domanda 4 - 3 punti
```
main:
    x = 9
    stampa(9):
        stampa(8):
            stampa(6):
                stampa(4):
                    stampa(2):
                        stampa(0):
                            STOP
                    "2 "
                "4 "
            "8 "
```

``
Risultato: 2 4 8 
``

## Domanda 5 - 3 punti
```
main:
    a = 30
    b = 15
    int *punt = &a          //punt->a
    int *prp = &b           //prp->b
    f(punt,prp)
    f(*p,*&rp):
        int *tmp = p        //tmp = p(punt) ->a
        p = rp              //p(punt) = rp(prp) -> b
        *tmp = 20           //tmp = p(punt) -> a = 20 DEF
        *p = 10             //p(punt) = rp(prp) -> b = 10 DEF
        rp = p              //rp(prp) = p(punt) -> a DEF
    "20 20 10 20"
```

``
Risultato: 20 20 10 20
``

## Domanda 6 - 3 punti

#### _main.cc_

```
a = 2
void fun()
    "La variabile a vale:" 2
int fun(int);

main:
    a = 3
    a = fun(a) = 5
        return 3 + 2 = 5
    fun():
        "La variabile a vale:" 2
    return 0

fun(int x):
    return x + a;
```

#### _fun.cc_

```
extern int a
fun():
    "La variabile a vale:" a
```

##### **Risultato:**

* La compilazione va a buon fine.
* -
* L'eseguibile stampa:
  "La variabile a vale:" 2  
  "La variabile a vale:" 2  


## Domanda 7 - 6 punti

```cpp
bnode* bst_search(bnode* b, int k)
{
    if(b != NULL)
    {
        if(b->key == k)
            return b;
        else if(b->key > k)
            return bst_search(b->left,k);
        else if(b->key < k)
            return bst_search(b->right,k);
    }
    return NULL
}
```

## Domanda 8 - 4 punti

```cpp
void stampa_ric(lista p)
{
    if(p != NULL)
    {
        cout << head(p);
        stampa_ric(tail(p));
    }
}
```

## Domanda 9 - 6 punti

```cpp
bool path(node*n , tipo_inf v)
{
    bool result;

    if(n != NULL)
    {
        if(get_info(n) == v)
            return true;
        else
            while(n != NULL && result == false)
            {
                result = path(get_firstChild(n));
                result = result | path(get_nextSibling(n));
                n = get_nextSibling(n);
            }
            return result;
    }
    return false;
}
```