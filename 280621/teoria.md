# Prova scritta di programmazione 2 - 17/02/2022

## Domanda 1 - 2 punti
- [ ] a
- [x] b
- [x] c
- [ ] d

## Domanda 2 - 2 punti
- [ ] a
- [x] b
- [x] c
- [ ] d

## Domanda 3 - 2 punti
- [x] a
- [ ] b
- [x] c
- [x] d

## Domanda 4 - 3 punti
```
???                
```

``
Risultato: ???
``

## Domanda 5 - 3 punti
```
main:
    int a = [0,3,4,6,6,5]
    int* p = &a[2]          //p->a[2]
    a[*p] = a[*(p-1)]       //a[a[2]] = a[a[1]] -> a[4] = a[3] = 6
                            // a = [0,3,4,6,6,5]
    p = p-2                 //p = a[0]
    *p = 18                 //a[0] = 18
                            // a = [18,3,4,6,6,5]
    *(p+1) = *(p+3)         //a[1] = a[3] = 6
                            // a = [18,6,4,6,6,5]
```

``
Risultato: "18 6 4 6 6 5"
``

## Domanda 6 - 3 punti

I breakpoint sono dei punti nel codice, corrispondenti ad una determinata riga di codice, che indicano al debugger in quale punto fermarsi per iniziare la visualizzazione del codice. I watch operator non son altro che la rappresentazione delle variabili in maniera visuale, per far vedere il loro comportamento nell'andamento del codice. Se imposto un breakpoint a rica 10 e a riga 11 vi è `a = 15` il debug comincerà dalla riga 10 e quando si arriva alla riga 11, vedremo nei watchoperators a = 15 per indicare che a contiene il valore 15.

## Domanda 7 - 5 punti

```cpp
bnode* bst_search(bst b, char* p)
{
    if(b == NULL)
        return NULL;
    while(b != NULL)
    {
        if(strcmp(b->key,p)==0)
            return b;
        else if(strcmp(b->key,p)<0)
            bst_search(b->left,p);
        else if(strcmp(b->key,p)>0)
            bst_search(b->right,p);
    }
    return NULL
}
```

## Domanda 8 - 6 punti

```cpp
void print_subtree(bst b, int x, int y)
{
    b = bst_search(b,x);

    if(b != NULL)
    {
        if(get_value(b)<y)
            cout << get_value(b);
        
        print_subtree(b->left,get_value(b->left));
        print_subtree(b->right,get_value(b->right));
    }
}
```

## Domanda 9 - 6 punti

```cpp
lista split(lista& l)
{
    lista p = l;
    lista nl = NULL;
    ...
}
```