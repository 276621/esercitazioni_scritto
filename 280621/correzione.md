# Prova 17/02/22
## _Punteggio finale_

| Esercizio | Punteggio |
|:---------:|:---------:|
|     1     |     2     |
|     2     |     1     |
|     3     |     2     |
|     4     |     /     |
|     5     |     0     |
|     6     |     3     |
|     7     |     5     |
|     8     |     6     |
|     9     |     3     |
|   Totale  |     **22**    |